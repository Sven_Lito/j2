import Generator from 'yeoman-generator';
import {
  getConfigDir,
  getRelativeConfigDir
} from '../utils';

class ModelGenerator extends Generator {
  constructor(args, options) {
    super(args, options);

    this.argument('name', {
      model: String,
      required: true
    });

    this.destinationDir = getConfigDir('model');
  }

  generateModel() {
    const name = this.options.name;

    const directories = getRelativeConfigDir('model', ['model']);

    const templatePath = this.templatePath('Model.js.template');
    const destinationPath = this.destinationPath(`${this.destinationDir}/${name}.js`);
    const templateVars = {
      name,
      directories
    };

    this.fs.copyTpl(templatePath, destinationPath, templateVars);
  }

  end() {
    this.log('🔥 Model created!');
  }
}

module.exports = ModelGenerator;
