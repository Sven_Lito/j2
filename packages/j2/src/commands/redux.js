import spawn from 'cross-spawn';
import shell from 'shelljs';

const redux = (opts) => {
  const args = opts.commands ? [] : [opts];
  const command = 'redux';

  if (!shell.which(command)) {
    console.log(`cannot find command: ${command}`);
    process.exit(1);
  }

  spawn(command, args, { shell: true, stdio: 'inherit' });
};

export default redux;
