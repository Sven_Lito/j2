import path from 'path';
import fs from 'fs';
import pkgDir from 'pkg-dir';
import merge from 'lodash.merge';

const rootPath = pkgDir.sync('.') || '.';

/**
 * Uppercase the first letter of a text
 * @param text {string}
 * @returns {string}
 */
export const uppercaseFirstLetter = text => `${text.charAt(0).toUpperCase()}${text.slice(1)}`;

/**
 * Parse `.graphqlrc` config file and retrieve its contents
 * @param filePath {string} The path of the config file
 * @returns {*}
 */
const parseConfigFile = (filePath) => {
  const config = JSON.parse(fs.readFileSync(filePath, 'utf8'));

  const directories = Object.keys(config.directories).reduce((data, directory) => {
    if (directory === 'source') {
      return {
        ...data,
        [directory]: `${rootPath}/${config.directories[directory]}`
      };
    }

    return {
      ...data,
      [directory]: `${config.directories.source}/${config.directories[directory]}`
    };
  }, {});

  return {
    ...config,
    directories: {
      ...config.directories,
      ...directories
    }
  };
};

/**
 * Get the `.j2rc` config file
 * @returns {object} The content of the config
 */
export const getCreateJ2Config = () => {
  // Use default config
  const defaultFilePath = path.resolve(`${__dirname}/j2rc.json`);

  const config = parseConfigFile(defaultFilePath);

  try {
    // Check if there is a `.j2rc` file in the root path
    const customConfig = parseConfigFile(`${rootPath}/.j2rc`);

    merge(config, customConfig);

    // If it does, extend default config with it, so if the custom config has a missing line
    // it won't throw errors
    return config;
  } catch (err) {
    // Return the default config if the custom doesn't exist
    return config;
  }
};

/**
 * Get a directory from the configuration file
 * @param directory {string} The name of the directory, e.g. 'source'/'mutation'
 * @returns {string} The directory path
 */
export const getConfigDir = directory => getCreateJ2Config().directories[directory];

/**
 * Get the relative path directory between two directories specified on the config file
 * @param from {string} The calling directory of the script
 * @param to {[string]} The destination directories
 * @returns {string} The relative path, e.g. '../../src'
 */
export const getRelativeConfigDir = (from, to) => {
  const config = getCreateJ2Config().directories;

  return to.reduce((directories, dir) => {
    const relativePath = path.posix.relative(config[from], config[dir]);

    return {
      ...directories,
      [dir]: relativePath === '' ? '.' : relativePath
    };
  }, {});
};

/**
 * Camel cases text
 * @param text {string} Text to be camel-cased
 * @returns {string} Camel-cased text
 */
export const camelCaseText = text =>
  text.replace(/(?:^\w|[A-Z]|\b\w|\s+)/g, (match, index) => {
    if (+match === 0) {
      return '';
    }

    return index === 0 ? match.toLowerCase() : match.toUpperCase();
  });
