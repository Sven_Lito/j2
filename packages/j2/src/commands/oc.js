import spawn from 'cross-spawn';
import shell from 'shelljs';


const oc = (opts) => {
  const args = opts.commands ? [] : [opts];
  const command = 'oc';

  if (!shell.which(command)) {
    console.log(`cannot find command: ${command}`);
    console.log('Please run `brew install openshift-cli`');
    process.exit(1);
  }

  spawn('oc', args, { shell: true, stdio: 'inherit' });
};

export default oc;
