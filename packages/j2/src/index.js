import 'babel-polyfill';

import program from 'commander';

import pkg from '../package.json';
import { init } from './commands';
import { verifyYeoman } from './utils';

program.version(pkg.version);

program
.command('init <project>')
.alias('i')
.description('Create a new MicroService project')
.action(async (project) => {
  await verifyYeoman();
  init(project);
});

program.parse(process.argv);

if (process.argv.length <= 2) {
  program.help();
}
