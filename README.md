# J2-cli



- Create a microservices based on [microservice-seed-node](https://bitbucket.org/jeniusdeux/seed-microservice-node).
- Generate redux component based on [redux-cli]().


## Install

```sh
npm install --global j2-cli
```

## Update
```sh
npm update --global j2-cli
```

It can generate:

- [x] HapiJS based Microservices
- [x] Models;
- [x] Plugins;

### Examples

#### `init`

Create a Microservice according to our seed project.

  ```sh
  j2 init microservice
  ```
  
  
#### `generate`

Generate a Model.

  ```sh
  j2 generate --model cards  #produces: ./src/api/cards
  ```

Generate a Plugin.

  ```sh
  j2 generate --plugin cards
  ```


## Configuration file

You may customize the folders that the generated files will be created on by using a `.j2rc` file on the root folder with the following content:

```json
{
  "directories": {
    "source": "src",
    "model": "models",
    "plugin": "api",
  }
}
```

## How to contribute

1. Fork this repository;
2. Clone the forked version of j2:

	```
	git clone git@bitbucket.org:Sven_Lito/j2.git	```

3. Install [lerna/lerna](https://github.com/lerna/lerna)
4. 
	```
	npm install --global lerna@prerelease
	```

4. Install the main package depencies
	```
	yarn
	```
	or
	```
	npm i
	```

5. Bootstrap all packages

	```
	lerna bootstrap
	```
	
	This will install all dependencies of all `subpackages` and link them properly


6. Link the `generator` package

	```
	cd packages/generator
	npm link
	```

7. Watch all packages (j2 and generator)

	```
	npm run watch
	```

8. Create a new branch
	```
	git checkout -b feature/snapsnap
	```

9. Make your changes

10. Run the CLI with your changes

	```
	node packages/j2/dist --help
	```

11. Commit your changes and push your branch

	```
	git add .
	git commit -m 'more stuff for j2'
	git push origin feature/snapsnap
	```

12. Open your Pull Request
13. Have your Pull Request merged! 😎


## License

MIT © [Sven Lito](https://bitbucket.org/Sven_Lito/j2)
