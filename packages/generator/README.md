# generator-microservice

This repo provides a seed for dockerised microservices for Node.js, describing minimum requirements and standards. It is based on our [Seed Microservice](https://bitbucket.org/jeniusdeux/seed-microservice-node).

# TODO

- Include CircleCI
- Include Codecov


## Sample APIs

This project exposes some sample APIs to provide useful starting points for talking to REST backends, SOAP backends, MQ backends and public SaaS providers.

| API | Demonstrates |
|-----|--------------|
| GET [/cards](./src/api/cards/) | Fetching cards from a REST backend. |
| Get Card PAN | (WIP SOAP backend) |
| Change Card Limit | (WIP MQ backend) |
| TODO | (WIP Public SaaS API) |


## Usage

To run this microservice as an interactive docker container

```bash
docker run -it -p 3000:3000 -p 4000:4000 mdlingenium/generator-microservice
```

To run it in a detached mode use the `-d` flag

```bash
docker run -d -p 3000:3000 -p 4000:4000 mdlingenium/generator-microservice
```

- Runs the app server on `http://localhost:3000`
- Swagger UI runs on `http://localhost:4000/docs`
- Swagger spec runs on `http://localhost:4000/swagger.yml`

## Commands

The following commands are useful when working with this repo:

| Command | Usage |
|---------|-------|
| `make build` | Builds `mdlingenium/generator-microservice:latest`, also created an image with the current short git SHA. |
| `make test` | Runs integration tests. |
| `make deploy` | Pushes to the Docker Hub. Make sure you're logged in with `docker login`. |
| - | - |
| `yarn` | Install dependencies. |
| `npm start` | Start the server in prodution mode. |
| `npm run dev` | Start the server in development mode, watching for changes. |
| `npm run debug` | Start the server with a debugger attached. |
| `npm test` | Run all specs matching the pattern `*.spec.js`. |
| `npm test:debug` | Run the specs, initially paused in the debugger. |
| `npm run cov` | Generate code coverage reports to the screen and to the `./artifacts/coverage` directory. |


## Application Structure

```
├── Dockerfile
├── README.md
├── src
│   └── api                     # Each API endpoint should be in its own folder in the api folder
│       ├── get-cards-spec.js   # Unit tests should be adjacent to the files they test
│       └── get-cards.js
├── config
│   └── index.js                # Setup and configuration that are read from environment variables
├── docker-compose.test.yml     # A Docker compose overlay to run the integration tests
├── docker-compose.yml          # Spins up the server with mocked backend dependencies
├── index.js                    # Main server entrypoint
├── makefile                    # Common build commands for Dockerfiles
├── package.json                # Dependencies, metadata and npm commands
├── test-client
│   └── README.md
└── yarn.lock                   # Ensures deterministic dependency resolution

```

## Setup

Install:

- [NVM](https://github.com/creationix/nvm)
- [Yarn](https://yarnpkg.com/en/docs/install):

Use Node LTS, which is currently 6:

```bash
nvm install --lts
nvm use --lts
```

## Developing

Install dependencies:

```bash
yarn
```

Run in development mode with:

```bash
npm run dev
```

This will monitor the source code, reload on changes and run tests when any files are saved.

To debug the code, run:

```bash
npm run debug
```

This will run the server with the debugger attached, open `localhost:123/debug` to inspect. The server starts initially paused on the first line.

## Capabilities and Frameworks

|Capability|Module|
|------------------|-----------|
|Server framework|[`hapi`](https://hapijs.com/) as a web server with [`inert`](https://github.com/hapijs/inert) for static content, [`vision`](https://github.com/hapijs/vision) for templating, [`joi`](https://github.com/hapijs/joi) for validation and [`boom`](https://github.com/hapijs/boom) for errors.|
|API documentation|[`hapi-swagger`](https://github.com/glennjones/hapi-swagger) automatically builds swagger specs from code annotations|
|Clients|[`request`](https://github.com/request/request) for standard HTTP client, [`soap`](https://github.com/vpulim/node-soap) for SOAP clients, `xyz` for MQ client|
|Logging|[`bunyan`](https://github.com/trentm/node-bunyan) to provide logging formats (timestamp, etc.) and log levels|
|Hot reload|[`backpack`](https://github.com/palmerhq/backpack) | hot reloading is baked in and based on webpack.
|Coding standards|[`eslint`](http://eslint.org/) with the popular [`eslint-config-standard`](http://standardjs.com/) standard |
|Test framework|[`mocha`](https://mochajs.org/) as a rest runner, [`chai`](http://chaijs.com/) for assertations, [`sinon`](http://sinonjs.org/) as the mocking library|
|Code coverage|[`nyc`](https://github.com/istanbuljs/nyc) for test coverage reporting|

## Coding Guidelines

- **Coding Style**: Run `npm run lint` to check the code.
- **Pull Requests**: Any pull requests which drop code coverage are likely to be rejected, test your code!
- **Error Handling**: Handlers should catch exceptions and use `boom`. Normal functions should throw exceptions or use error callbacks if they are async.
- **Data Model**: Data received from the client should be validated, sanitised and new objects created before passing over to the backend.
- **Business Logic**: If writing a fair amount of business logic that should be unit tested, you should write the code in a modular way that can easily be unit tested.

## Healthchecks

The service exposes a healthcheck API at `/healthcheck`, with the following payload:

```json
{
  "status": "ok",
  "version": {current project version extracted from package.json}
}
```

## Testing

### Unit Tests
The unit tests are all **kept adjacent** to the files which they test.

Tests will run automatically when the project is run with `npm run dev`. To manually run the in-proc unit tests, run:

```bash
npm test
```

This will run `mocha`, executing tests in any file which matches the pattern `*.spec.js`.

### Debugging Tests

The easiest way to debug tests is to first make sure that only the test in question is running, use the [`only`](https://mochajs.org/#exclusive-tests) trick and add a 'debugger' statement:

```js
describe('Some fixture', () => {
  it.only('should do something' => {
    //  Only this test'll run....
  });
});
```

Now quickly spin up a debugger:

```
$ npm run test:debug
Debugger listening on port 9229.
Warning: This is an experimental feature and could change at any time.
To start debugging, open the following URL in Chrome:
    chrome-devtools://devtools/remote/serve_file/@60cd6e859b9f557d2312f5bf532f6aec5f284980/inspector.html?experiments=true&v8only=true&ws=127.0.0.1:9229/249e5c8e-a3aa-4200-8a93-b499da24d9be
Debugger attached.
```

Your test files will not all be shown till the mocha function runs, so you might need to continue in the debugger till your hit your `debugger` statement.

### Integration Tests

Integration tests are in the `test-client` folder, these tests will test this service and it's mocked dependencies end to end. Integration tests can be run with:

```bash
docker-compose -f docker-compose.yml -f docker-compose.test.yml
```

This spins up the server, mock backend and test client in the `test-client/` folder and runs integration tests against the system.

### Code Coverage


Code coverage reports can be generated with:

```bash
npm run cov
```

Reports are written to the screen and to the `artifacts/coverage` directory.

## Healthchecks

This service exposes a basic healthcheck at `/healthcheck`:

```

GET /healthcheck/ HTTP/1.1
host: localhost

HTTP/1.1 200 OK
Content-Type: application/json; charset=utf-8
Content-Length: 33

{"status":"ok","version":"0.0.1"}
```
