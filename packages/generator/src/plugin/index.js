import Generator from 'yeoman-generator';
import chalk from 'chalk';
import {
  getConfigDir,
  getRelativeConfigDir
} from '../utils';

const tic = chalk.green('✓');

class PluginGenerator extends Generator {
  constructor(args, options) {
    super(args, options);

    this.argument('name', {
      plugin: String,
      required: true
    });

    this.destinationDir = getConfigDir('plugin');
  }

  generatePlugin() {
    const name = this.options.name;

    const directories = getRelativeConfigDir('plugin', ['plugin']);

    const templatePath = this.templatePath('Plugin.js.template');
    const destinationPath = this.destinationPath(`${this.destinationDir}/${name}/index.js`);
    const templateVars = {
      name,
      directories
    };

    this.fs.copyTpl(templatePath, destinationPath, templateVars);
  }

  end() {
    this.log(`${tic} Plugin created!`);
    this.log('🔥 Dont forget load your plugin!');
  }
}

module.exports = PluginGenerator;

