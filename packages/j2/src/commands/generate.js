import spawn from 'cross-spawn-promise';

function parseOptions(opts) {
  const availableOptions = ['model', 'plugin'];

  // Check if any commands was provided
  const anyCommandsProvided = Object.keys(opts).some(option =>
    availableOptions.indexOf(option) !== -1,
  );

  let options = opts;

  // If not, use the default options
  if (!anyCommandsProvided) {
    options = {
      model: true,
      plugin: true,
      ...options,
    };
  }

  const {
    model,
    plugin,
  } = options;

  return {
    model: model || false,
    plugin: plugin || false,
  };
}

function generate(name, options) {
  // Parse all arguments
  const parsedOptions = parseOptions(options);
  // Get only the chose arguments
  const chosenOptions = Object.keys(parsedOptions).filter(opt => !!parsedOptions[opt]);

  chosenOptions.forEach(async (option) => {
    const payload = [`j2:${option}`, name];

    await spawn('yo', payload, {
      shell: true,
      stdio: 'inherit',
    });
  });
}

export default generate;
