import Generator from 'yeoman-generator';
import shell from 'shelljs';
import chalk from 'chalk';
import ora from 'ora';
import path from 'path';
import fs from 'fs';

import logo from '../j2-logo';

const seedRepo = 'https://git.ecommchannels.com/jenius2/seed-microservice-node.git';

const tic = chalk.green('✓');
const tac = chalk.red('✗');

class AppGenerator extends Generator {

  constructor(args, options) {
    super(args, options);

    shell.config.silent = true;

    this.argument('name', {
      type: String,
      required: true
    });

    this.dir = path.resolve(this.options.name);
  }

  initializing() {
    this.spinner = ora();
    this._printJ2Logo(); // eslint-disable-line
  }

  prompting() {
    const { name } = this.options;
    const serviceName = name.replace(/\s/g, '-');
    const prompts = [
      {
        type: 'input',
        name: 'projectName',
        message: 'What would you like to call this microservice?',
        default: name
      },
      {
        type: 'input',
        name: 'description',
        message: 'Describe your MicroService',
        default: `Jenius 2 ${serviceName}`
      }
    ];

    return this.prompt(prompts)
    .then((props) => {
      props.serviceId = props.projectName; // eslint-disable-line
      this.props = props;
    });
  }

  cloneSeed() {
    this.spinner.start();

    this._validateDirectory(); // eslint-disable-line

    this.spinner.text = 'Creating a new Jenius 2 Microservice...';

    const done = this.async();
    const repository = seedRepo;
    const { name } = this.options;
    const { description } = this.props;
    const cmd = `git clone ${repository} ${this.dir}`;

    shell.exec(cmd, { silent: true, async: true }, () => {
      this.spinner.stop();

      this.log(`${tic} Microservice ${name} created.`);


      shell.cd(this);
      shell.ls('*')
      .forEach((file) => {
        shell.sed('-i', /seed-microservice-node/g, `${name}`, file);
        shell.sed('-i', /Seed Node.js microservice/g, `${description}`, file);
      });
      done();
    });
  }

  _validateDirectory() {
    try {
      fs.lstatSync(this.dir).isDirectory();

      this._logAndExit( // eslint-disable-line
        `${tac} Directory "${this.options.name}" already exists!`,
      );

      return false;
    } catch (err) {
      return true;
    }
  }

  _printJ2Logo() {
    this.log(chalk.blue(logo));
  }

  installModules() {
    this.spinner.start();
    this.spinner.text = 'Installing dependencies...';

    const done = this.async();
    const command = 'yarn';

    this.spawnCommand(command, [], {
      stdio: 'ignore',
      cwd: this.dir
    })
    .on('close', () => {
      this.spinner.stop();

      this.log(`${tic} Dependencies installed! 😎`);

      done();
    });
  }

  _cleanDir() {
    shell.cd(this.dir);
    shell.rm('-rf', '.git');
  }

  _logAndExit(message) {
    this.spinner.stop();
    this.log(message);
    process.exit(1);
  }

  end() {
    this._cleanDir(); // eslint-disable-line
    this.log(`${tic} Your new project has been created! 🔥`);
  }

}

module.exports = AppGenerator;
